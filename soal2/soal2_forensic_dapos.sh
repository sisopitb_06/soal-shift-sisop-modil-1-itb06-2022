#!/bin/bash

awk  -F":" ' BEGIN{}
    { if (NR != 1 ){
        hour [$3] ++
        }
    }
END {
    total = 0
    line = 0
    for (i in hour){
        total = total + hour[i]
        line ++
    }
    rata_rata = total/line
    print "Rata-rata serangan adalah sebanyak", rata_rata , "requests per jam"
} ' log_website_daffainfo.log >>  forensic_log_website_daffainfo_log/rata-rata.txt |

awk  -F":" ' BEGIN{}
    { if (NR != 1 ){
        ip [$1] ++ }
    }
END {
    mod = 0
    index_mod = 0
    for (i in ip){
        if(ip[i] >= mod ){
            mod = ip[i]
            index_mod = i
        }
    }
    print "IP yang paling banyak mengakses server adalah:", index_mod , "sebanyak", mod, "requests"
} ' log_website_daffainfo.log >>  forensic_log_website_daffainfo_log/result.txt |

awk  -F":" ' BEGIN {}
    /curl/ { ++ jumlah_req_curl}
    END {
        print "\n"
        print "Ada", jumlah_req_curl , "requests yang menggunakan curl sebagai user-agent"
        
    } ' log_website_daffainfo.log >>  forensic_log_website_daffainfo_log/result.txt |

awk -F":" '{ if ($3 == 02) { print $1 } }' log_website_daffainfo.log | head | uniq >>  forensic_log_website_daffainfo_log/result.txt


