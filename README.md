# soal-shift-sisop-modil-1-ITB06-2022

## Soal 1
Membuat program yang dapat menjalankan command `dl N` yang dapat mendownload gambar sebanyak N kali dan user dapat menjalankan command `att` untuk memonitoring percobaan login terhadap akun dia

a. Membuat program register.sh untuk mendaftarkan akun baru ke dalam program dan simpan database akun yang terdaftar pada file ./users/user.txt. Lalu mem buat juga program untuk login dengan program yang diberi nama main.sh

b. Membuat kriteria password sesuai dengan ketentuan yang diberikan, yaitu minimal 8 karakter, memiliki minimal 1 huruf kapital dan 1 huruf kecil, alphanumeric, dan tidak boleh sama dengan username

c. Membuat log.txt yang berisi percobaan login dan register dengan format MM/DD/YY hh:mm:ss MESSAGE. Dengan message seperti berikut:

- Ketika mencoba register dengan username yang sudah terdaftar, maka message pada log adalah REGISTER: ERROR User already exists

- Ketika percobaan register berhasil, maka message pada log adalah REGISTER: INFO User USERNAME registered successfully

- Ketika user mencoba login namun passwordnya salah, maka message pada log adalah LOGIN: ERROR Failed login attempt on user USERNAME

- Ketika user berhasil login, maka message pada log adalah LOGIN: INFO User USERNAME logged in

d. Membuat command yang dapat dijalankan ketika user berhasil login, yaitu command
 - dl N dengan N adalah jumlah gambar yang akan didownload  dari https://loremflickr.com/320/240 dengan jumlah sesuai dengan yang diinginkan oleh user dan gambar tersemput disimpan dalam folder dengan format nama YYYY-MM-DD_USERNAME, serta gambar-gambar di dalam folder tersebut akan diberi nama PIC_XX dengan nomor yang saling berurutan. Ketika semua gambar telah berhasil didownload, dimasukkan, dan diberi nama yang sesuai dengan permintaan, maka folder tersebut akan di zip dengan nama yang sesuai dengan folder dan akan diberi password sesuai dengan password user yang memberikan perintah untuk menjalankan command. Apabila sudah terdapat zip dengan nama yang sama saat ingin memasukkan foto ke dalam zip, maka masukkan foto yang terbaru tersebut ke dalam zip tersebut dengan meng unzip zip tersebut dan meng zip kembali dengan password sesuai dengan user

- att adalah command untuk menghitung jumlah percobaan login baik yang berhasil maupun tidak dari user yang sedang login saat ini.

### Jawaban
a. Sebelum menjalankan program register.sh, program tersebut harus membuat directory ./users terlebih dahulu, setelah itu disusul dengan membuat file ./users/user.txt dan log.txt dengan kondisi apabila file tersebut belum terbuat. Apabila membuat file yang sudah terbuat, maka akan terdapat overwrite. Perintah-perintah tersebut dijalankan menggunakan command

`mkdir -p ./users`

`test -f ./users/user.txt || touch ./users/user.txt`

`test -f log.txt || touch log.txt`

Lalu, memasukkan username dan passowrd yang ingin didaftarkan ke ./users/user.txt

`read -p 'Username: ' username`

`read password`

`echo "$username $password" >> ./users/user.txt`

Kemudian, user dapat memasukkan username dan password yang sudah terdaftar pada main.sh menggunakan command yang sudah disediakan di program tersebut, yakni

`read -p 'Username: ' username`

`read password`

b. Untuk menyembunyikan input password dari user di register.sh dan main.sh adalah menggunakan command berikut

`stty -echo`

`read password`

`stty echo`

Dalam membuat persyaratan password yang sesuai kriteria, program kami menggunakan regex dan akan melakukan looping terus menerus hingga password yang dimasukkan sudah sesuai dengan kriteria

`until [[ ! "$password" == "$username" &&  ${#password} -ge 8 && "$password" == *[A-Z]* && "$password" == *[a-z]* && "$password" == *[0-9]* ]];`

`do`

`echo "password belum sesuai kriteria"`

`echo "Password baru -"`

`stty -echo`

`read password`

`stty echo`

`done`

c. Untuk mendapatkan data timestamp yang akan ditampilkan dalam log.txt, dibuatlah function timestamp() yang bisa mendapatkan secara otomatis timestamp ketika program dijalankan

`timestamp() {`

`date +"%m/%d/%Y %T"`

`}`

Ketika program register.sh dijalankan, program akan terus looping mendapatkan username yang tersedia di ./users/user.txt dan setiap kali looping akan memasukan message pada log.txt

`until ! grep  -q "$username" ./users/user.txt;`

`do`

`echo "Username sudah terdaftar"`

`read -p 'Username baru: ' username`

`echo "$timeStamp REGISTER: ERROR User already exists" >> log.txt`

`done`

Ketika username berhasil didaftarkan, maka message yang bersangkutan akan ditampilkan pula dalam log.txt

`echo "$timeStamp REGISTER: INFO User $username registered succesfully" >> log.txt`

Untuk program main.sh, akan ada pengecekan terlebih dahulu mengenai username yang dimasukkan apakah sudah sesuai dengan password yang terdaftar pada akun tersebut

`exists=$(awk -v username="${username}" -v password="${password}" '$1==username && $2==password {print 1}' ./users/user.txt)`

Pada variable exist ini, variable akan memiliki nilai 1 apabila username dan password akun yang dimasukkan sudah sesuai

Apabila exist masih belum bernilai 1, maka program akan selalu meminta user untuk memasukkan username dan passowrd yang sesuai, beserta program akan membuat catatan dalam log.txt bahwa terdapat percobaan login pada username yang dimasukkan. Berikut merupakan command dari instruction yang disebutkan

`until [[ "$exists" == 1 ]];`

`do`

`echo "$timeStamp LOGIN: ERROR Failde login attempt on user $username" >> log.txt`

`echo "Username atau Password masih salah"`

`read -p 'Username: ' username`

`echo  "Password - "`

`stty -echo`

`read password`

`stty echo`

`exists=$(awk -v username="${username}" -v password="${password}" '$1==username && $2==password {print 1}' ./users/user.txt)`

`done`

Apabila sudah berhasil login, maka akan terdapat message pada log.txt menggunakan command berikut

`echo "$timeStamp LOGIN: INFO User $username logged in" >> log.txt`

d. Untuk command pertama, yaitu att, command tersebut akan menelusuri percobaan login yang berhasil dan tidak berhasil terhadap akun yang menjalankan command tersebut melalui file log.txt karena di situ tersimpan segala percobaan login dan register. Berikut merupakan command yang digunakan untuk melihat berapa kali percobaan login terhadap akun tersebut 

`awk -v username="${username}" '$3=="LOGIN:" && ($6==username || $10==username) {++n} END {print "login attempt = " n}' log.txt`

Untuk command yang kedua, kami perlu mendownload zip dan unzip file di ubuntu menggunakan `sudo apt install zip unzip`.
Setelah didownload,  untuk menyimpan gambar tersebut harus disimpan dalam zip yang memiliki format nama dengan permintaan soal menggunakan ``folder_name=`date +%F`"_$username"``. Namun apabila foler telah dibuat, maka cukup unzip zip file yang sebelumnya telah dibuat menggunakan password yang sesuai dengan password user bersangkutan, setelah itu ambilah index penomoran file terakhir yang disimpan dalam zip yang sudah di unzip

`if [[ ! -f "./$folder_name.zip" ]]`

`then`

`mkdir -p "./$folder_name"`

`else`

`unzip -p "$password" "./$folder_name.zip"`

``file= `ls "$folder_name" -r | head -n 1``

`index=$(awk -F"_" '{print $2}' <<< "$file")`

`fi`

Setelah dibentuk sebuah folder baru atau mendapatkan penomoran index terakhir file terakhir, dimulali download file pada link yang tersedia menggunakan looping dengan penomoran dimulasi posisi index sekarang dijumlahkan satu s/d batas file yang diminta oleh user

`for ((numFile=index+1; numFile<=index+q; numFile++))`

`do`

`wget -O "$folder_name/PIC_$numFile" https://loremflickr.com/320/240`

`done`

Setelah berhasil didownload menggunakan format nama yang sesuai dan dimasukkan ke dalam folder, masukkan kembali semua file yang telah didownload tersebut ke dalam zip menggunakan password yang sesuai dengan password user yang memberikan command

`zip -r -P "$password" "$folder_name.zip" "$folder_name"`

`rm -rf "$folder"`

## Soal 2
2.	Pada tanggal 22 Januari 2022, website `https://daffa.info` di hack oleh seseorang yang tidak bertanggung jawab. Sehingga hari sabtu yang seharusnya hari libur menjadi berantakan. Dapos langsung membuka log website dan menemukan banyak request yang berbahaya. Bantulah Dapos untuk membaca log website `https://daffa.info` Buatlah sebuah script awk bernama "soal2_forensic_dapos.sh" untuk melaksanakan tugas-tugas berikut:

a.	Buat folder terlebih dahulu bernama `forensic_log_website_daffainfo_log`.

b.	Dikarenakan serangan yang diluncurkan ke website `https://daffa.info` sangat banyak, Dapos ingin tahu berapa rata-rata request per jam yang dikirimkan penyerang ke website. Kemudian masukkan jumlah rata-ratanya ke dalam sebuah file bernama `ratarata.txt` ke dalam folder yang sudah dibuat sebelumnya.

c.	Sepertinya penyerang ini menggunakan banyak IP saat melakukan serangan ke website `https://daffa.info`, Dapos ingin menampilkan IP yang paling banyak melakukan request ke server dan tampilkan berapa banyak request yang dikirimkan dengan IP tersebut. Masukkan outputnya kedalam file baru bernama `result.txt` kedalam folder yang sudah dibuat sebelumnya.

d.	Beberapa request ada yang menggunakan user-agent ada yang tidak. Dari banyaknya request, berapa banyak requests yang menggunakan user-agent curl? Kemudian masukkan berapa banyak requestnya kedalam file bernama `result.txt` yang telah dibuat sebelumnya.

e.	Pada jam 2 pagi pada tanggal 22 terdapat serangan pada website, Dapos ingin mencari tahu daftar IP yang mengakses website pada jam tersebut. Kemudian masukkan daftar IP tersebut kedalam file bernama `result.txt` yang telah dibuat sebelumnya.

### Jawaban

a.	Pada persoalan ini, kita menggunakan command pada ubuntu secara manual dengan menggunakan `mkdir` dengan nama yang telah ditentukan yaitu “forensic_log_website_daffainfo_log”.
```sh
mkdir forensic_log_website_daffainfo_log
```

b.	Sebelum bisa mengakses nilai yang ada pada data log, dilakukan syarat pembacaan awk sebagai berikut. 
```sh
awk  -F":" 'BEGIN{}
    {action}
END {action}' filename >>  saving destination
```
Diawal AWK command terdapat `-F":"` yang berfungsi untuk menentukan batas tiap kolom data dari file `log_website_daffainfo.log`
Pada persoalan berikut diperintahkan untuk mencari rata-rata dari total penyerangan tiap jamnya. Oleh karena itu dibutuhkan pengambilan data untuk tiap jamnya. Diawali dengan baris action sebagai berikut
```sh
{ if (NR != 1 ){
        hour [$3] ++
        }
    }
``` 
Hal ini berguna untuk mengambil data tiap jam yang berada di kolom 3 data log dengan mengecualikan baris pertama data `log_website_daffainfo.log`. Setelah terkumpul semua data tiap jamnya maka pada AWK END dilakukan perulangan untuk mendapatkan total jumlah sebagai berikut. 
```sh
END {
    total = 0
    line = 0
    for (i in hour){
        total = total + hour[i]
        line ++
    }
    rata_rata = total/line
    print "Rata-rata serangan adalah sebanyak", rata_rata , "requests per jam"
}
```
Total akan disimpan pada variable `total` yang jumlahnya akan terus berulang hingga semua data dari hour terhitung. Disaat yang sama setiap perulangan akan menambahkan nilai dari variable `line` yang digunakan sebagai berapa banyak serangan yang terjadi. Setelah perulangan mencapai batas akhir, maka nilai rata-rata akan didapat dari nilai variable `total` dibagi dengan variable `line` yang kemudian disimpan pada variable `rata_rata`. 
Nilai tersebut akan disimpan pada file `rata-rata.txt` yang berada di folder `forensic_log_website_daffainfo_log` dengan code `log_website_daffainfo.log >>  forensic_log_website_daffainfo_log/rata-rata.txt` diakhir AWK command.

Dan berikut adalah hasil dari program awk setelah dijalankan.

![](./img/rata.png)

c.	Pada persoalan ini memiliki algoritma yang hampir sama dengan persoalan B. namun pada soal ini diperintahkan untuk mencari ip dengan total akses terbanyak ke server. Dari persoalan ini dapat diatasi dengan mencari ip yang memiliki line terbanyak pada log. Oleh karena itu dilakukan pengambilan data ip terlebih dahulu dengan menggunakan `ip[$1]++`. Menggunakan `$1` dikarenakan nilai ip yang diperoleh berada di kolom 1 dari data log. Code yang digunakan sebagi berikut.
```sh
{ if (NR != 1 ){
        ip [$1] ++
        }
    }
```
Kemudian dilakukan perulangan dengan menggunakan beberapa variable seperti `mod` dan `index_mod`. Perulangan dilakukan sesuai dengan banyak ip pada data yang kemudian dilakukan looping serta pembandingan dengan persyaratan `if`. Di dalam persyaratan `if` akan dibandingkan pada ip yang pertama masuk akan dicek apakah nilainya lebih besar dengan nilai dari `mod`. 
```sh
mod = 0
index_mod = 0
for (i in ip){
    if(ip[i] >= mod ){
        mod = ip[i]
        index_mod = i
    }
}
```
Apabila banyak serangan lebih besar dari `mod` awal yang bernilai 0, maka nilai dari `ip[i]` yang pertama akan menggantikan nilai variable `mod` dan `index_mod` akan berisi identitas ip tadi. Persyaratan `if` terus dilakukan hingga mendapatkan nilai maksimal. Setelah mencapai batas akhir, alamat ip yang tersimpan di `index_mod` beserta total serangannya yang tersimpan di variabel `mod` akan dicetak dan disimpan pada file `result.txt` pada folder `forensic_log_website_daffainfo_log` dengan code `log_website_daffainfo.log >>  forensic_log_website_daffainfo_log/result.txt` diakhir AWK command.

d.	Pada persoalan ini diminta untuk melakukan penghitungan jumlah request yang menggunakan curl sebagai user-agentnya. Penyelesaiannya digunakan menggunakan tanda `/(character)/` yang digunakan untuk mencari karakter yang sesuai dari data log. Oleh karena itu pada fungsi AWK ini cukup menggunakan code sebagai berikut. 
```sh
/curl/ { ++ jumlah_req_curl}
```
Hal tersebut digunakan untuk mencari karakter curl disetiap line dari data log. Total dari karakter `curl` yang ditemukan akan disimpan di variable `jumlah_req_curl`. Setelah mendapatkan nilai totalnya. Nilai variable `jumlah_req_curl` akan di simpan di  file `result.txt` pada folder `forensic_log_website_daffainfo_log` dengan code `log_website_daffainfo.log >>  forensic_log_website_daffainfo_log/result.txt` diakhir AWK command .

e.	Persoalan terkahir diminta untuk melakukan pengecekan alamat ip mana saja yang melakukan penyerangan di pukul 02.dengan demikian cukup menggunakn fungsi AWK dengan melakukan pengecekan di kolom 3 yang dimana merupakan kolom dari nilai jam penyerangan. Pengecekan dilakukan dengan cara persyaratan if sebagai berikut.
```sh 
{ if ($3 == 02) { print $1 } }
```
Setelah mendapatkan line mana saja yang memiliki nilai 02 pada kolom ketiganya. Maka dilakukan pencetakan pada kolom satu saja dengan “ print $1”. Hal ini dikarenakan soal hanya meminta alamat ip yang menyerang saat jam tersebut saja. Supaya ip yang didapat tidak bertumpuk atau muncul beberapa kali pada tiap line, maka digunakan `uniq` untuk menampilkan ip yang berbeda saja. 
```sh
| uniq 
```
Setalah itu, alamat ip akan dicetak dan di simpan pada file “result.txt” pada folder “forensic_log_website_daffainfo_log” dengan code `log_website_daffainfo.log | head | uniq >>  forensic_log_website_daffainfo_log/result.txt` diakhir AWK command.

Dan berikut adalah hasil dari program awk setelah dijalankan.

![](./img/result.png)

## Soal 3
Membuat program monitoring resource untuk monitoring ram dan monitoring size suatu directory. Untuk ram menggunakan command `free -m`. Untuk disk menggunakan command `du -sh <target_path>`. Kemudian Mencatat semua metrics yang didapatkan dari hasil `free -m`. Untuk hasil `du -sh <target_path>` catat size dari path directory tersebut. Untuk target_path yang akan dimonitor adalah /home/{user}/.

a. Masukkan semua metrics ke dalam suatu file log bernama metrics_{YmdHms}.log. {YmdHms} adalah waktu disaat file script bash kalian dijalankan. Misal dijalankan pada 2022-01-31 15:00:00, maka file log yang akan tergenerate adalah metrics_20220131150000.log.

b. Script untuk mencatat metrics diatas diharapkan dapat berjalan otomatis pada setiap menit.

c. Kemudian, buat satu script untuk membuat agregasi file log ke satuan jam. Script agregasi akan memiliki info dari file-file yang tergenerate tiap menit. Dalam hasil file agregasi tersebut, terdapat nilai minimum, maximum, dan rata-rata dari tiap-tiap metrics. File agregasi akan ditrigger untuk dijalankan setiap jam secara otomatis. Berikut contoh nama file hasil agregasi metrics_agg_2022013115.log dengan format metrics_agg_{YmdH}.log

d. Karena file log bersifat sensitif pastikan semua file log hanya dapat dibaca oleh user pemilik file.

### Jawaban

a. Pada soal 3a kami menjalankan command free -m untuk mendapatkan hasil dari monitoring ram berupa total, used, free, shared, buff/cache, dan available. Kemudian untuk mendapatkan hasil dari monitoring disk menggunakan command `du -sh /home/{user}/.` Dan mendapatkan output masing-masing seperti gambar dibawah ini

![](./img/freem.png)

kemudian setelah mengetahui output dari `free-m` dan `du-sh` kami dapat membuat script untuk mencatat semua output yang telah dijalnkan dengan command nya dan semua hasil tersebut akan di taruh pada file yang bernama metrics_{YmsHMs}.log. dan untuk yang pertama akan dijelaskan codenya, yang pertama dibuat adalah meng define user agar variabel user bisa dibaca ketika dipanggil, dan menggunakan command `mkdir -p` untuk pertama kali membuat folder untuk menyimpan hasil log, tapi apabila sudah ada maka `-p` akan mengecheck dan tidak membuat folder baru. code diberikan seperti dibawah ini.

```sh
user=$(whoami)
mkdir -p /home/"$user"/log\
```
Kemudian kami akan menampilkan output seperti format pada soal menggunakan command `echo` yang nantinya akan diletakan pada file hasil metrics yang akan dibuat dan pada path yang telah ditentukan menggunakan penamaan file yang juga sesuai pada saat scriptnya dijalankan.

```sh
echo -e "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,pathsize" >> /home/"$user"/log/metrics_$(date +%Y%m%d%H%M%S).log
```
Setelah itu maka sekarang akan dibuat command awk untuk mengambil value dari command yang telah dijalankan, untuk mengambil dan mengeprint value kami menggunakan command `printf` dan menyesuaikan dengan kedudukan argumen berapa yang akan diambil valuenya, command ini berlaku untuk semua awk dalam mengambil value dengan format `Mem` `Swap` `disk`

```sh
ram="$(free -m | awk '/Mem:/ {printf "%s,%s,%s,%s,%s,%s", $2,$3,$4,$5,$6,$7}')"
Ram="$(free -m | awk '/Swap:/ {printf "%s,%s,%s",$2,$3,$4}')"
disk="$(du -sh /home/opay | awk '{printf "%s,%s",$2,$1}')"
```
Setelah berhasil mendapatkan value dan di print, sekarang kami akan menyimpan hasil output tersebut pada file metrics yang sudah ditentukan dengan path yang juga telah ditentukan yaitu sebagai berikut

```sh
echo -e "$ram,$Ram,$disk" >> /home/"$user"/log/metrics_`date +%Y%m%d%H%M%S`.log 
```
Dan berikut adalah salah satu contoh dari hasil metrics yang automatis terbuat apabila script dijalankan.

![](./img/metrics.png)

b. Kemudian ketika soal a telah dikerjakan, kami para praktikan diminta untuk menjalankan script tersebut secara automatis menggunakan `cron` langkah pertama yaitu mengetikan command `crontab -e` untuk membuka file cron dan membuat pengaturan kapan script tersebut akan dijalankan, karena diminta untuk setiap menit maka menggunakan `* * * * *` yang artinya script berjalan setiap menit dan kemudian memasukan path file script yang akan dijalankan.

![](./img/cron.png)
