#!/bin/bash


file=($(awk '{print}' <<< `ls "/home/opay/log" -r | head -n 60`))

total=()
used=()
free=()
shared=()
buff=()
available=()
swap_total=()
swap_used=()
swap_free=()
path=()
path_size=()

for var in "${file[@]}";
do
	total+=($(awk -F, 'NR==2 {print $1}' /home/opay/log/"$var"))
	used+=($(awk -F, 'NR==2 {print $2}' /home/opay/log/"$var"))
	free+=($(awk -F, 'NR==2 {print $3}' /home/opay/log/"$var"))
	shared+=($(awk -F, 'NR==2 {print $4}' /home/opay/log/"$var"))
	buff+=($(awk -F, 'NR==2 {print $5}' /home/opay/log/"$var"))
	available+=($(awk -F, 'NR==2 {print $6}' /home/opay/log/"$var"))
	swap_total+=($(awk -F, 'NR==2 {print $7}' /home/opay/log/"$var"))
	swap_used+=($(awk -F, 'NR==2 {print $8}' /home/opay/log/"$var"))
	swap_free+=($(awk -F, 'NR==2 {print $9}' /home/opay/log/"$var"))
	path+=($(awk -F, 'NR==2 {print $10}' /home/opay/log/"$var"))
	path_size+=($(awk -F, 'NR==2 {print $11}' /home/opay/log/"$var"))
done

echo "${total[@]}"
