#!/bin/bash

user=$(whoami)
 
mkdir -p /home/"$user"/log

echo -e "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,pathsize" >> /home/"$user"/log/metrics_$(date +%Y%m%d%H%M%S).log
ram="$(free -m | awk '/Mem:/ {printf "%s,%s,%s,%s,%s,%s", $2,$3,$4,$5,$6,$7}')"
Ram="$(free -m | awk '/Swap:/ {printf "%s,%s,%s",$2,$3,$4}')"
disk="$(du -sh /home/opay | awk '{printf "%s,%s",$2,$1}')"




echo -e "$ram,$Ram,$disk" >> /home/"$user"/log/metrics_`date +%Y%m%d%H%M%S`.log 
