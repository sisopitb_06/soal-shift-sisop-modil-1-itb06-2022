#! /bin/bash/

mkdir -p ./users
test -f ./users/user.txt || touch ./users/user.txt
test -f log.txt || touch log.txt

#input username
read -p 'Username: ' username

timestamp() {
	date +"%m/%d/%Y %T"
}

timeStamp=$(timestamp)



until ! grep  -q "$username" ./users/user.txt;
do
	echo "Username sudah terdaftar"
	read -p 'Username baru: ' username
	echo "$timeStamp REGISTER: ERROR User already exists" >> log.txt
done

echo "$timeStamp REGISTER: INFO User $username registered succesfully" >> log.txt

echo  "Password - "
stty -echo
 read password
stty echo
	until [[ ! "$password" == "$username" &&  ${#password} -ge 8 && "$password" == *[A-Z]* && "$password" == *[a-z]* && "$password" == *[0-9]* ]];
	do
		echo "password belum sesuai kriteria"
		echo "Password baru -"
		stty -echo
		 read password
		stty echo
	done

echo "$username $password" >> ./users/user.txt

